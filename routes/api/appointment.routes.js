const Appointment = require('../../models/appointment.model');
const express = require('express');
const router = express.Router();
// create and save
router.post ('/appointments',(req,res) => {
  // Validate request
     if(!req.body.name) {
         return res.status(400).send({
             message: "Appointment content can not be empty"
         });
     }

     // Create an Appointment
     const appointment = new Appointment({
         name: req.body.name || "Untitled Appointment",
         phone: req.body.phone,
         time: req.body.time,
         desc: req.body.desc,
         fromReact: req.body.fromReact
     });

     // Save Appointment in the database
     appointment.save()
     .then(data => {
         res.send(data);
     }).catch(err => {
         res.status(500).send({
             message: err.message || "Some error occurred while creating the Appointment."
         });
     });
});

// Retrieve and return all
router.get('/appointments',(req, res) => {
  Appointment.find()
    .then(appointments => {
        res.send(appointments);
    }).catch(err => {
        res.status(500).send({
            message: err.message || "Some error occurred while retrieving appointments."
        });
    });
});

// Find one by ID
router.get('/appointments/:appointmentId', (req, res) => {
  Appointment.findById(req.params.appointmentId)
   .then(appointment => {
       if(!appointment) {
           return res.status(404).send({
               message: "Appointment not found with id " + req.params.appointmentId
           });
       }
       res.send(appointment);
   }).catch(err => {
       if(err.kind === 'ObjectId') {
           return res.status(404).send({
               message: "Appointment not found with id " + req.params.appointmentId
           });
       }
       return res.status(500).send({
           message: "Error retrieving appointment with id " + req.params.appointmentId
       });
   });
});

// Update a appointment identified by the appointmentId in the request
router.put('/appointments/:appointmentId', (req, res) => {
    // Validate Request
    if(!req.body.name) {
        return res.status(400).send({
            message: "Appointment content can not be empty"
        });
    }
    // Find appointment and update it with the request body
    Appointment.findByIdAndUpdate(req.params.appointmentId, {
      name: req.body.name || "Untitled Appointment",
      phone: req.body.phone,
      time: req.body.time,
      desc: req.body.desc
    }, {new: true})
    .then(appointment => {
        if(!appointment) {
            return res.status(404).send({
                message: "Appointment not found with id " + req.params.appointmentId
            });
        }
        res.send(appointment);
    }).catch(err => {
        if(err.kind === 'ObjectId') {
            return res.status(404).send({
                message: "Appointment not found with id " + req.params.appointmentId
            });
        }
        return res.status(500).send({
            message: "Error updating appointment with id " + req.params.appointmentId
        });
    });
});

// Delete
router.delete('/appointments/:appointmentId',(req, res) => {
  Appointment.findByIdAndRemove(req.params.appointmentId)
     .then(appointment => {
         if(!appointment) {
             return res.status(404).send({
                 message: "Appointment not found with id " + req.params.appointmentId
             });
         }
         res.send({message: "Appointment deleted successfully!"});
     }).catch(err => {
         if(err.kind === 'ObjectId' || err.name === 'NotFound') {
             return res.status(404).send({
                 message: "Appointment not found with id " + req.params.appointmentId
             });
         }
         return res.status(500).send({
             message: "Could not delete appointment with id " + req.params.appointmentId
         });
     });
});

module.exports=router

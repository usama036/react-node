import React from "react";

export class EditRow extends React.Component {
	render() {
		var appointment = this.props.appointment;
		var onSaveEvent = this.props.onSaveEvent;
		let onDelEvent = this.props.onDelEvent;
		let onCellChange = this.props.onCellChange;

		return (
			<tr>
				<td>
					<input
						type="text"
						_id={appointment._id}
						name="name"
						defaultValue={appointment.name}
						onChange={onCellChange}
						className="input"
					/>
				</td>
				<td>
					<input
						type="text"
						_id={appointment._id}
						name="phone"
						defaultValue={appointment.phone}
						onChange={onCellChange}
						className="input"
					/>
				</td>
				<td>
					<input
						type="text"
						_id={appointment._id}
						name="time"
						defaultValue={appointment.time}
						onChange={onCellChange}
						className="input"
					/>
				</td>
				<td>
					<input
						type="text"
						_id={appointment._id}
						name="desc"
						defaultValue={appointment.desc}
						onChange={onCellChange}
						className="input"
					/>
				</td>
				<td>
					<button onClick={onSaveEvent} className="button-success pure-button">
            <i className="material-icons">done</i>
					</button>
					<button onClick={onDelEvent} className="button-error pure-button">
            <i className="material-icons">delete</i>
					</button>
				</td>
			</tr>
		);
	}
}

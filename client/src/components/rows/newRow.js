import React from "react";

export class NewRow extends React.Component {
  render() {
    var onSaveEvent = this.props.onSaveEvent;
    let cellChange = this.props.onCellChange;

    return (
      <tr>
        <td>
          <input
            type="text"
            name="name"
            value={this.props.name}
            onChange={cellChange}
            className="input"
          />
        </td>
        <td>
          <input
            type="text"
            name="phone"
            value={this.props.phone}
            onChange={cellChange}
            className="input"
          />
        </td>
        <td>
          <input
            type="text"
            name="time"
            value={this.props.time}
            onChange={cellChange}
            className="input"
          />
        </td>
        <td>
          <input
            type="text"
            name="desc"
            value={this.props.desc}
            onChange={cellChange}
            className="input"
          />
        </td>
        <td>
          <button onClick={onSaveEvent} className="button-success pure-button">
            <i className="material-icons">add_circle</i>
          </button>
        </td>
      </tr>
    );
  }
}

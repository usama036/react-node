import React from "react";

export class AppointmentRow extends React.Component {
  render() {
    var onEditEvent = this.props.onEditEvent;
    var onDelEvent = this.props.onDelEvent;
    var appointment = this.props.appointment;

    return (
      <tr>
        <td>{appointment.name}</td>
        <td>{appointment.phone}</td>
        <td>{appointment.time}</td>
        <td>{appointment.desc}</td>
        <td>
          <button onClick={onEditEvent} className="button-edit pure-button">
            <i className="material-icons">border_color</i>
          </button>
          <button onClick={onDelEvent} className="button-error">
            <i className="material-icons">delete</i>
          </button>
        </td>
      </tr>
    );
  }
}

import React from "react";
import { Appointment } from "../containers/appointment";
import "../styles/AppointmentList.css";

export class AppointmentList extends React.Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  render() {
    let rows = [];

    let rowDel = this.props.onRowDel;
    let onRowAdd = this.props.onRowAdd;
    let onRowSave = this.props.onRowSave;
    let onFilterChange = this.props.onFilterChange;
    let onColumnSort = this.props.onColumnSort;
    let buttonHeaderStyling = this.props.setButtonHeaderStyle;
    let filter = this.props.filterValue;

    this.props.appointments.forEach(function(appointment) {
      rows.push(
        <Appointment
          appointment={appointment}
          onDelEvent={rowDel}
          onRowSave={onRowSave}
          key={appointment._id}
        />
      );
    });
    var emptyAppointment = { _id: "", name: "", phone: "", time: "", desc: "" };
    rows.push(
      <Appointment
        appointment={emptyAppointment}
        newMode={true}
        key="newAppointment"
        onRowAdd={onRowAdd}
      />
    );
    return (
      <div className="appointment-list">
        <div className="title-list">
          <h1>Appointment List</h1>
        </div>
        <table className="table-list">
          <thead className="table-head">
            <tr>
              <th>
                <button
                  name="name"
                  onClick={onColumnSort}
                  className={buttonHeaderStyling("nameSort")}
                >
                  {" "}
                  Name{" "}
                </button>
              </th>
              <th>
                <button
                  name="phone"
                  onClick={onColumnSort}
                  className={buttonHeaderStyling("phoneSort")}
                >
                  {" "}
                  Phone{" "}
                </button>
              </th>
              <th>
                <button
                  name="time"
                  onClick={onColumnSort}
                  className={buttonHeaderStyling("timeSort")}
                >
                  {" "}
                  Time{" "}
                </button>
              </th>
              <th>
                <button
                  name="desc"
                  onClick={onColumnSort}
                  className={buttonHeaderStyling("descSort")}
                >
                  {" "}
                  Description{" "}
                </button>
              </th>
              <th className="LastColumn" />
            </tr>
          </thead>
          <tbody>
            <tr className="pure-table-odd">
              <td>
                <input
                  name="name"
                  type="text"
                  onChange={onFilterChange}
                  value={filter.name}
                  className="input"
                />
              </td>
              <td>
                <input
                  name="phone"
                  type="text"
                  onChange={onFilterChange}
                  value={filter.phone}
                  className="input"
                />
              </td>
              <td>
                <input
                  name="time"
                  type="text"
                  onChange={onFilterChange}
                  value={filter.time}
                  className="input"
                />
              </td>
              <td>
                <input
                  name="desc"
                  type="text"
                  onChange={onFilterChange}
                  value={filter.desc}
                  className="input"
                />
              </td>
              <td>
                <i className="search-icon material-icons">search</i>
              </td>
            </tr>
            {rows}
          </tbody>
        </table>
      </div>
    );
  }
}

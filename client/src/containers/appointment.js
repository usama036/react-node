import React from "react";
import { AppointmentRow } from "../components/rows/appointmentRow";
import { EditRow } from "../components/rows/editRow";
import { NewRow } from "../components/rows/newRow";

export class Appointment extends React.Component {
	constructor(props) {
		super(props);

		this.state = {
			editMode: false,
			name: "",
			phone: "",
			time: "",
			desc: ""
		};
		var appointment = this.props.appointment;

		if (this.props.editMode) this.state.editMode = true;
		if (this.props.newMode) this.state.newMode = true;
		// this.state.appointment = this.props.appointment;
		this.state.appointment = appointment;
		this.handleRowEdit = this.handleRowEdit.bind(this);
		this.handleRowSave = this.handleRowSave.bind(this);
		this.handleRowCreate = this.handleRowCreate.bind(this);
		this.newRowCellChange = this.newRowCellChange.bind(this);
		this.editOnCellChange = this.editOnCellChange.bind(this);
	}

	handleRowCreate() {
		let appointment = {
			name: this.state.name,
			phone: this.state.phone,
			time: this.state.time,
			desc: this.state.desc
		};
		this.props.onRowAdd(appointment);
		this.setState({
			name: "",
			phone: "",
			time: "",
			desc: ""
		});
	}

	handleDeleteClick = () => {
		this.props.onDelEvent(this.props.appointment);
	};

	newRowCellChange(evt) {
		var item = {
			name: evt.target.name,
			value: evt.target.value
		};
		this.setState({ [item.name]: item.value });
	}

	editOnCellChange(evt) {
		let appointment = this.state.appointment;
		appointment[evt.target.name] = evt.target.value;
		this.setState({
			appointment: appointment
		});
	}

	handleRowEdit() {
		this.setState({ editMode: true });
	}

	handleRowSave() {
		this.setState({ editMode: false });
		// Different than state.name etc to be able to reset it if needed
		this.props.onRowSave(this.state.appointment);
	}

	// Lifecycle methods
	componentWillReceiveProps(nextProps){
        if(nextProps.appointment !== this.props.appointment){
            this.setState({appointment:nextProps.appointment});
        }
    }

	render() {
		let appointment = this.props.appointment;
		let editMode = this.state.editMode;
		let newMode = this.state.newMode;
		let rowDel = this.handleDeleteClick;
		let newRowCellChange = this.newRowCellChange;
		let editOnCellChange = this.editOnCellChange;

		let rendering;

		if (newMode) {
			rendering = (
				<NewRow
					onSaveEvent={this.handleRowCreate}
					onCellChange={newRowCellChange}
					name={this.state.name}
					phone={this.state.phone}
					time={this.state.time}
					desc={this.state.desc}
				/>
			);
		} else {
			if (editMode) {
				rendering = (
					<EditRow
						appointment={appointment}
						onDelEvent={rowDel}
						onSaveEvent={this.handleRowSave}
						onCellChange={editOnCellChange}
					/>
				);
			} else {
				rendering = (
					<AppointmentRow
						appointment={appointment}
						onDelEvent={rowDel}
						onEditEvent={this.handleRowEdit}
					/>
				);
			}
		}
		return rendering;
	}
}

import React from "react";
import axios from "axios";
import { AppointmentList } from "../components/appointmentList";
import { propAscSort, propDescSort, inputFilter } from "../utils/appointments";

export class Main extends React.Component {
  constructor(props) {
    super(props);
    this.state = {};
    this.state.appointments = [
      {
        _id: "1",
        name: "Person One",
        phone: "04 78 97 87 65",
        time: "15:00 15/07/18",
        desc: "Paint job"
      },
      {
        _id: "2",
        name: "People Two",
        phone: "05 74 74 74 74",
        time: "10:00 05/12/18",
        desc: "Wheel fixation"
      },
      {
        _id: "3",
        name: "People Three",
        phone: "05 44 44 44 54",
        time: "01:12 25/08/19",
        desc: "Cleaning"
      }
    ];
    this.state.shownAppointments = this.state.appointments;
    this.state.filter = {
      name: "",
      phone: "",
      time: "",
      desc: ""
    };
    this.state.activeSort = "0";

    this.handleRowDel = this.handleRowDel.bind(this);
    this.handleRowAdd = this.handleRowAdd.bind(this);
    this.handleRowSave = this.handleRowSave.bind(this);
    this.handleColumnSort = this.handleColumnSort.bind(this);
    this.buttonHeaderStyling = this.buttonHeaderStyling.bind(this);
    this.handleFilterChange = this.handleFilterChange.bind(this);
  }

  // CSS methods
  buttonHeaderStyling(buttonName) {
    return (
      (this.state.activeSort === buttonName + "Asc"
        ? "button-secondary"
        : this.state.activeSort === buttonName + "Dsc"
          ? "button-tertiary"
          : "") + " button-sort"
    );
  }

  // Method used to filter and sort the list before rerendering it
  applyFilterSort = (appointments, activeSort) => {
    // Filter
    appointments = appointments.filter(
      inputFilter(
        this.state.filter.name,
        this.state.filter.phone,
        this.state.filter.time,
        this.state.filter.desc
      )
    );
    // Sort
    let order = activeSort.substr(-3);
    activeSort = activeSort.replace("Sort" + order, "");

    if (order !== "0") {
      if (order === "Asc") {
        appointments = appointments.sort(propAscSort(activeSort));
      } else if (order === "Dsc") {
        appointments = appointments.sort(propDescSort(activeSort));
      }
    }
    return appointments;
  };

  // Handling events methods
  handleRowDel(appointment) {
    let index = this.state.appointments.indexOf(appointment);
    let appointments = this.state.appointments;

    appointments.splice(index, 1);
    let shownAppointments = this.applyFilterSort(
      appointments,
      this.state.activeSort
    );

    // Delete on API
    let urlDelete = "http://localhost:5000/api/data/appointments/" + appointment._id;

    axios
      .delete(urlDelete, { appointment })
      .then(
        this.setState({
          appointments: appointments,
          shownAppointments: shownAppointments
        })
      )
      .catch(err => {
        console.log(JSON.stringify(err.config));
      });
  }

  handleRowAdd(appointment) {
    // Create on API
    axios
      .post("http://localhost:5000/api/data/appointments", {
        name: appointment.name,
        phone: appointment.phone,
        time: appointment.time,
        desc: appointment.desc,
        fromReact: true
      })
      .then(res => {
        appointment._id = res.data._id;
      })
      .catch(err => {
        JSON.stringify(err.config);
      });

    // Update State and the data shown filtered/sorted
    let appointments = this.state.appointments;
    appointments.push(appointment);

    let shownAppointments = this.applyFilterSort(
      appointments,
      this.state.activeSort
    );

    this.setState({
      appointments: appointments,
      shownAppointments: shownAppointments
    });
  }

  handleRowSave(appointment) {
    // Update on API
    axios
      .put("http://localhost:5000/api/data/appointments/" + appointment._id, {
        name: appointment.name,
        phone: appointment.phone,
        time: appointment.time,
        desc: appointment.desc
      })
      .then()
      .catch(err => {
        console.log(err.response);
      });
  }

  handleFilterChange(evt) {
    let filters = this.state.filter;
    filters[evt.target.name] = evt.target.value;

    let shownAppointments = this.state.appointments.filter(
      inputFilter(filters.name, filters.phone, filters.time, filters.desc)
    );

    // Sort
    let activeSort = this.state.activeSort;

    let order = activeSort.substr(-3);
    activeSort = activeSort.replace("Sort" + order, "");
    if (order !== "0") {
      if (order === "Asc") {
        shownAppointments = shownAppointments.sort(propAscSort(activeSort));
      } else if (order === "Dsc") {
        shownAppointments = shownAppointments.sort(propDescSort(activeSort));
      }
    }

    this.setState({ filter: filters, shownAppointments: shownAppointments });
  }

  handleColumnSort(evt) {
    let buttonName = evt.target.name + "Sort";

    let shownAppointments = this.state.shownAppointments;

    if (this.state.activeSort === buttonName + "Asc") {
      shownAppointments = shownAppointments.sort(propDescSort(evt.target.name));
      this.setState({
        activeSort: buttonName + "Dsc",
        shownAppointments: shownAppointments
      });
    } else {
      shownAppointments = shownAppointments.sort(propAscSort(evt.target.name));
      this.setState({
        activeSort: buttonName + "Asc",
        shownAppointments: shownAppointments
      });
    }
  }

  // Lifecycle Methods
  componentDidMount() {
    // Read All on API
    axios
      .get("http://localhost:5000/api/data/appointments")
      .then(res => {
        let shownAppointments = this.applyFilterSort(
          res.data,
          this.state.activeSort
        );
        this.setState({
          appointments: res.data,
          shownAppointments: shownAppointments
        });
      })
      .catch(err => {
        console.log("API GET : Error " + err.response);
      });
  }

  render() {
    return (
      <AppointmentList
        appointments={this.state.shownAppointments}
        filterValue={this.state.filter}
        onRowDel={this.handleRowDel}
        onRowAdd={this.handleRowAdd}
        onRowSave={this.handleRowSave}
        onFilterChange={this.handleFilterChange}
        onColumnSort={this.handleColumnSort}
        setButtonHeaderStyle={this.buttonHeaderStyling}
      />
    );
  }
}

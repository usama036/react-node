export const propAscSort = propName => (a, b) => {
  let aProp = a[propName],
    bProp = b[propName];

  return aProp || bProp
    ? !aProp ? -1 : !bProp ? 1 : aProp.localeCompare(bProp)
    : 0;
};

export const propDescSort = propName => (b, a) => {
  let aProp = a[propName],
    bProp = b[propName];

  return aProp || bProp
    ? !aProp ? -1 : !bProp ? 1 : aProp.localeCompare(bProp)
    : 0;
};

export const inputFilter = (name, phone, time, desc) => appointment => {

  return (
    appointment.name.toLowerCase().includes(name.toLowerCase()) &&
    appointment.phone.toLowerCase().includes(phone.toLowerCase()) &&
    appointment.time.toLowerCase().includes(time.toLowerCase()) &&
    appointment.desc.toLowerCase().includes(desc.toLowerCase())
  );
};

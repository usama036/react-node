const mongoose = require('mongoose');

const AppointmentSchema = mongoose.Schema({
  name: String,
  phone: String,
  time: String,
  desc: String,
  fromReact: Boolean
}, {
  timestamps: true
});

module.exports = mongoose.model('Appointment', AppointmentSchema);
